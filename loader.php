<?php
/*
 * Plugin Name: Managed Word Required Plugin Loader
 * Plugin URI:
 * Description:
 * Version: 1.0
 * Author: Managed Word
 * Author URI: http://www.managedword.com
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */

require WPMU_PLUGIN_DIR . '/redis-cache/redis-cache.php';
require WPMU_PLUGIN_DIR . '/nginx-helper/nginx-helper.php';
